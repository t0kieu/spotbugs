module gitlab.com/gitlab-org/security-products/analyzers/spotbugs/v2

require (
	github.com/gosimple/slug v1.9.0
	github.com/sirupsen/logrus v1.7.0
	github.com/stretchr/testify v1.6.1
	github.com/termie/go-shutil v0.0.0-20140729215957-bcacb06fecae
	github.com/urfave/cli/v2 v2.3.0
	gitlab.com/gitlab-org/security-products/analyzers/command v1.1.0
	gitlab.com/gitlab-org/security-products/analyzers/common/v2 v2.22.1
	gitlab.com/gitlab-org/security-products/analyzers/report/v2 v2.1.0
	gitlab.com/gitlab-org/security-products/analyzers/ruleset v1.0.0
)

go 1.15
