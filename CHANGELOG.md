# spotbugs analyzer changelog

## v2.28.2
- Update ant to 1.10.10 (!97)
- Update glibc to 2.33-r0 (!97)
- Update Java8 to adoptopenjdk-8.0.292+10 (!97)
- Update Java11 to adoptopenjdk-11.0.11+9 (!97)

## v2.28.1
- Bumps spotbugs to [v4.2.3](https://github.com/spotbugs/spotbugs/blob/4.2.3/CHANGELOG.md) (!96)
  - Inconsistency in the description of `DLS_DEAD_LOCAL_INCREMENT_IN_RETURN`, `VO_VOLATILE_INCREMENT` and `QF_QUESTIONABLE_FOR_LOOP`
  - Should issue warning for SecureRandom object created and used only once
  - False positive `OBL_UNSATIFIED_OBLIGATION` with try with resources
  - `SA_LOCAL_SELF_COMPUTATION` bug
  - False positive `EQ_UNUSUAL` with record classes
- Update gradle to [v6.8.3](https://docs.gradle.org/6.8/release-notes.html) (!96)
- Update grails to [v4.0.10](https://github.com/grails/grails-core/releases/tag/v4.0.10) (!96)
- Update sbt to [v1.4.9](https://github.com/sbt/sbt/releases/tag/v1.4.9) (!96)
- Update scala to [v2.13.5](https://github.com/scala/scala/releases/tag/v2.13.5) (!96)

## v2.28.0
- Add support for Kotlin projects (!93)

## v2.27.0
- Update Dockerfile to support Open Shift (!95)

## v2.26.0
- Update report dependency in order to use the report schema version 14.0.0 (!94)

## v2.25.3
- Update spotbugs to v4.2.2 (https://github.com/spotbugs/spotbugs/blob/4.2.2/CHANGELOG.md) (!92)

## v2.15.2
- Update Java8 to adoptopenjdk-8.0.282+8 (!89)
- Update Java11 to adoptopenjdk-11.0.10+9 (!89)
- Update gradle to [6.8.1](https://docs.gradle.org/6.8/release-notes.html) (!89)
  - This version of Gradle removes TLS protocols v1.0 and v1.1 from the default list of allowed protocols
- Update grails to [4.0.6](https://github.com/grails/grails-core/releases/tag/v4.0.6) (!89)
- Update sbt to [1.4.6](https://github.com/sbt/sbt/releases/tag/v1.4.6) (!89)

## v2.15.1
- Update common to `v2.22.1` which fixes a CA Certificate bug when analyzer is run more than once (!88)

## v2.15.0
- Update spotbugs to v4.2.0 (!85)
- Update sbt to v1.4.5 (!85)

## v2.14.1
- Update common to v2.22.0 (!78)
- Update urfave/cli to v2.3.0 (!78)

## v2.14.0
- Update spotbugs to v4.1.4, find-sec-bugs to v1.11.0 (!80)
- Update gcc to 10.2.0-4 (!80)
- Update ant to 1.10.9 (!80)
- Update Java8 to adoptopenjdk-8.0.275+1 (!80)
- Update Java11 to adoptopenjdk-11.0.9+101 (!80)
- Update gradle to 6.7.1 (!80)
- Update grails to 4.0.5 (!80)
- Update sbt to 1.4.3 (!80)
- Update scala to 2.13.4 (!80)
- Update golang dependencies logrus to 1.7.0, urfave/cli to 1.22.5 (!80)

## v2.13.6
- Update common to v2.21.3 (!79)

## v2.13.5
- Update common to v2.21.0 (!77)

## v2.13.4
- Fix unhandled error when setting ANT_HOME (!62)

## v2.13.3
- Match `adoptopenjdk-11.x` in asdf when using major_version `11` (!70 @widerin)

## v2.13.2
- Update `metadata.ScannerVersion` to match spotbugs version `4.1.2` (!68)

## v2.13.1
- Update golang dependencies (!67)

## v2.13.0
- Update glibc to v2.32-r0 (!64)
- Update gcc to v10.2.0-2 (!64)
- Update spotbugs to v4.1.2 (!64)
- Update gradle to v6.6.1 (!64)
- Update grails to v4.0.4 (!64)
- Update sbt to v1.3.13 (!64)
- Update scala to v2.13.3 (!64)
- Update golang dependencies to latest versions (!64)

## v2.12.0
- Switch from sdkman to asdf sdk manager (!56)

## v2.11.0
- Add `scan.start_time`, `scan.end_time` and `scan.status` to report (!59)

## v2.10.1
- Upgrade go to version 1.15 (!57)

## v2.10.0
- Add scan object to report (!52)

## v2.9.0
- Switch to the MIT Expat license (!48)

## v2.8.1
- Update sdkman checksum (!50)
- Update Java 8 version (!50)
- Update Java 11 version (!50)

## v2.8.0
- Update logging to be standardized across analyzers (!47)

## v2.7.1
- Add self-signed ceritficates for java projects (!43)

## v2.7.0
- Add `COMPILE` environment variable to skip project compilation (!42)

## v2.6.1
- Remove `location.dependency` from the generated SAST report (!40)

## v2.6.0
- Bump spotbugs to 4.0.2, glibc to 2.31-r0, zlib to 1.3.11-4, grails to 4.0.3, maven to 3.6.3, sbt to 1.3.10, scala to 2.13.1

## v2.5.1
- Bump SDKMAN to 5.8.1+484, Java 8 to 8.0.252, Java 11 to 11.0.7

## v2.5.0
- Add `id` field to vulnerabilities in JSON report (!33)

## v2.4.2
- Fix bug incorrectly attributing SpotBugs vulnerability to FindSecBugs (!30)

## v2.4.1
- update gradle to 5.6 (!31)

## v2.4.0
- Add support for custom CA certs (!28)

## v2.3.6
- Fixes setting Java 11 after sdkman breaking update (!26)

## v2.3.5
- update java 8 defined in analyze.go to release 8.0.242 (!25)
- update java 11 defined in analyze.go to release 11.0.6 (!25)

## v2.3.4
- update java 8 to release 8.0.242 (!22)
- update java 11 to release 11.0.6 (!22)

## v2.3.3
- update sdkman to latest v5.7.4+362 (!21)

## v2.3.2
- update java 8 to patch release 8.0.232 (!16 @haynes)
- update java 11 to patch release 11.0.5 (!16 @haynes)
- update findsecbugs to 1.10.1 (!16 @haynes)
- update sdkman to latest version 5.7.4+362 (!16 @haynes)

## v2.3.1
- added `--batch-mode` to the default `MAVEN_CLI_OPTS` to reduce the logsize

## v2.3.0
- Add an environment variable `SAST_JAVA_VERSION` to specify which Java version to use (8, 11)
  - Default version remains Java 8
  - Specific versions of Java 8/11 can be set using `JAVA_8_VERSION` and `JAVA_11_VERSION`

## v2.2.3
- Fix report JSON compare keys (`cve`) non-uniqueness by including file path and line information into them

## v2.2.2
 - Switch primary `mvn`/`mvnw` build method from `compile` to `install`
 - Support builds on cross-referential multi-module projects

## v2.2.1
 - Update sdkman to latest version 5.7.3+337

## v2.2.0
 - Change default behavior to exit early with non-zero exit code if compilation fails
 - Introduce new `FAIL_NEVER` variable, to be set to `1` to ignore compilation failure

## v2.1.0
- Bump Spotbugs to [3.1.12](https://github.com/spotbugs/spotbugs/blob/3.1.12/CHANGELOG.md#3112---2019-02-28)
- Bump Find Sec Bugs to [1.9.0](https://github.com/find-sec-bugs/find-sec-bugs/releases/tag/version-1.9.0)

## v2.0.1
- Update common to v2.1.6

## v2.0.0
- Merge of the Maven, Gradle, Groovy and SBT analyzers with additional features:
  - Use the SpotBugs CLI for analysis.
  - Report correct source file paths.
  - Handle Maven multi module projects.
  - Only report vulnerabilities in source code files, ignore dependencies' libraries.
  - Add command line parameters and environment variable to set the paths of the Ant, Gradle, Maven, SBT and Java
    executables.
  - Add the `--mavenCliOpts` command line parameter and `MAVEN_CLI_OPTS` environment to pass arguments to Maven.
